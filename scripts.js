// Déplacer la vue vers un écran donné - slide the view to a particular ecran
function ecranSlide(n){
    $(".ecran").css('left','-'+n+'00%');
}
// Afficher une erreur dans la boîte à erreurs
function displayError(msg) {
    $('#error-text').html(msg);
    $('.error-area').css('display','block');
    ecranSlide(0);
}

// On loading page
window.onload = function(){
    var indexEcran = 0;
    $(document).keyup(function(e) {
        if (e.keyCode === 37) ecranSlide(0);
        if (e.keyCode === 39) ecranSlide(1);
    });
    translate(browserLanguage());
};


// declare the array that will contain all the SMSes
var smses;
// declare the object that will contain all the charts
var chartsReference = {};





// Load the SMS file
//https://www.geeksforgeeks.org/how-to-load-the-contents-of-a-text-file-into-a-javascript-variable/
//https://javascript.info/file
function loadFile(input) {
    $('.error-area').css('display','none');
    $('.success-area').css('display','none');
    let files = input.files;
    if (files.length == 0) { displayError('The file is empty'); return;}
    const file = files[0];
    let reader = new FileReader();

    reader.onload = (e) => {
        const file = e.target.result;
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(file, "text/xml");

        smsArray = xmlDoc.getElementsByTagName('sms')
        if (smsArray.length == 0) { displayError('The file contains no records or is not a valid XML file.'); return; }

        smses = Array();
        for (i=0; i<smsArray.length; i++) {
            sms = smsArray[i];
            smsDict = {
                'phoneNo' : sms.getAttributeNode('address').nodeValue,
                'timestamp' : sms.getAttributeNode('date').nodeValue,
                'body' : sms.getAttributeNode('body').nodeValue,
                'contactName' : sms.getAttributeNode('contact_name').nodeValue,
                'type' : sms.getAttributeNode('type').nodeValue
            } // type is 1 for received SMS, 2 for sent

            smses.push(smsDict);
        }

        try {
            defaultParams = {
                nTopContacts: 15,     // number of contacts after which they are grouped in "others"
                contacts: 'all',      // an array of the contacts to display - probably won't implement this since some charts can hide a dataset
                dateStart: 'default', // should be a timestamp
                dateEnd: 'default',   // should be a timestamp
                timeDivision: 'months' // for now only days, months, years are allowed
            };
            ecranSlide(1);
            loadCharts(defaultParams);
            $('.success-area').css('display','block');
        } catch(e) {
            displayError(e);
        }

    };
    reader.onerror = function(e) {
        alert(e.target.error.name);
        displayError(e);
    };
    reader.readAsText(file);
}


// When button pressed, apply filters
function reloadCharts() {
    var newParams = structuredClone(defaultParams);
    // get values from input fields
    nTopContacts = $('#nTopContacts').val();
    startDate = $('#dateStart').val()
    endDate = $('#dateEnd').val()
    // check validity
    if (nTopContacts != "") newParams.nTopContacts = Number(nTopContacts);
    if (startDate != "") newParams.dateStart = new Date(startDate).getTime();
    if (endDate != "") newParams.dateEnd = new Date(endDate).getTime();
    loadCharts(newParams);
}




function loadCharts(defaultParams) {

    const chartDataset = processData(smses, defaultParams);

    const config = configCharts(chartDataset);


    // destroy existing charts
    Object.keys(chartsReference).forEach(function(key,index) {
        chartsReference[key].destroy()
    });


    // Chart.js create chart
    // add each chart to a reference array
    chartsReference.topBarChart = new Chart( document.getElementById('chart-topContacts'), config.topBarChart );
    chartsReference.timeLineChart = new Chart( document.getElementById('chart-quantityOverTime'), config.timeLineChart );
    chartsReference.cumulatedChart = new Chart( document.getElementById('chart-stackedQtOverTime'), config.cumulatedChart );
    chartsReference.radarWeek = new Chart( document.getElementById('chart-radarWeek'), config.radarWeek );
    chartsReference.radarHour = new Chart( document.getElementById('chart-radarHour'), config.radarHour );
    //chartsReference.hourOfDay = new Chart( document.getElementById('chart-hourOfDay'), config.hourOfDay );

    /*var configtest = {
        type: 'wordCloud',
        data: {
            // text
            labels: ['Hello', 'world', 'normally', 'you', 'want', 'more', 'words', 'than', 'this'],
            datasets: [
            {
                label: 'DS',
                // size in pixel
                data: [90, 80, 70, 60, 50, 40, 30, 20, 10],
            },
            ],
        },
        options: {},
    };*/
    //chartsReference.wordCloud = new Chart( document.getElementById('chart-wordCloud'), configtest );

}
