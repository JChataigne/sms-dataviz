# sms-dataviz

Visualise your SMS statistics at https://jchataigne.gitlab.io/sms-dataviz/

Translate into a new language by editing files in the "dictionaries" folder.
