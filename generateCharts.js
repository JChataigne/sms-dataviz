// generate a hexadecimal color code based on a string hash
// taken from https://stackoverflow.com/questions/3426404
var stringToColour = function(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}


// Default parameters for all charts
Chart.defaults.color = "#dddddd";
Chart.defaults.borderColor = "#dddddd50";






function configCharts(chartDataset) {

    var chartsConfig = {};


    // TOP CONTACTS CHART
    var data = {
        labels: chartDataset.topBarChart.labels,
        datasets: [{
            data: chartDataset.topBarChart.data,
            backgroundColor: chartDataset.topBarChart.labels.reduce(function(list,contact) {list.push(stringToColour(contact)+'80'); return list;} ,[]),
            borderColor: chartDataset.topBarChart.labels.reduce(function(list,contact) {list.push(stringToColour(contact)); return list;} ,[]),
            borderWidth: 3
        }]
    };
    var config = {
        type: 'bar',
        data: data,
        options: {
            indexAxis: 'y',
            scales: {
                y: {
                    beginAtZero: true,
                    type: 'category',
                },
                x: {
                    position: 'top',
                }
            },
            plugins: {
                legend: { display: false }
            },
        },
    };
    chartsConfig.topBarChart = config;




    // TIMELINE CHART
    var data = {
        labels: chartDataset.timeLineChart.labels,
        datasets: Object.entries(chartDataset.timeLineChart.data).reduce(function(datasetsArray, contactData) {
            datasetsArray.push({
                label: contactData[0],
                data: Object.keys(contactData[1]).map(function(key) { return contactData[1][key] }),
                borderColor: stringToColour(contactData[0]),
                cubicInterpolationMode: 'monotone',
                tension: 0.4
            });
            return datasetsArray;
        }, [])
    };
    config = {
        type: 'line',
        data: data,
        options: {
            elements: {point: {radius: 0}},
            scales: {
                y: {
                    beginAtZero: true
                }
            },
        }
    };
    chartsConfig.timeLineChart = config;


    // CUMULATED CHART
    data = {
        labels: chartDataset.cumulatedChart.labels,
        datasets: Object.entries(chartDataset.cumulatedChart.data).reduce(function(datasetsArray, contactData) {
            datasetsArray.push({
                label: contactData[0],
                data: Object.keys(contactData[1]).map(function(key) { return contactData[1][key] }),
                borderColor: stringToColour(contactData[0]),
                backgroundColor: stringToColour(contactData[0]),
                fill: true,
                radius: 0
            });
            return datasetsArray;
        }, [])
    };
    config = {
        type: 'line',
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    stacked: true
                }
            }
        }
    };
    chartsConfig.cumulatedChart = config;



    // HOUR OF DAY CHART
    data = {
        labels: chartDataset.hourOfDay.labels,
        datasets: Object.entries(chartDataset.hourOfDay.data).reduce(function(datasetsArray, contactData) {
            datasetsArray.push({
                label: contactData[0],
                data: contactData[1].reduce(function(arr,hourCount,index){
                            arr.push({
                                x: index,
                                y: contactData[0],
                                r: hourCount
                            });
                            return arr;
                        }, []),
                borderColor: stringToColour(contactData[0]),
                backgroundColor: stringToColour(contactData[0]) + "80",
            });
            return datasetsArray;
        }, [])
    };
    config = {
        type: 'bubble',
        data: data,
        options: {
            scales: {
                x: {
                    type: 'category',
                    labels: chartDataset.hourOfDay.labels,
                },
                y: {
                    type: 'category',
                    labels: chartDataset.contactsList.slice(0, chartDataset.nTopContacts+1),
                }
            }
        }
    };
    chartsConfig.hourOfDay = config;



    // RADAR WEEK
    data = {
        labels: chartDataset.radarWeek.labels,
        datasets: Object.entries(chartDataset.radarWeek.data).reverse().reduce(function(datasetsArray, contactData) {
            datasetsArray.push({
                label: contactData[0],
                data: contactData[1].reduce(function(arr,hourCount,index){
                            arr.push({
                                x: index,
                                y: contactData[0],
                                r: hourCount
                            });
                            return arr;
                        }, []),
                borderColor: stringToColour(contactData[0]),
                backgroundColor: stringToColour(contactData[0]) + "80",
                fill: true,
            });
            return datasetsArray;
        }, [])
    };

    config = {
        type: 'radar',
        data: data,
        options: {
            scales: {
                r: {
                    //reverse: true,
                    stacked: true,
                    beginAtZero: true,
                    ticks: {
                        display: false
                    },
                },
            },
        }
    };
    chartsConfig.radarWeek = config;



    // RADAR HOUR OF DAY
    data = {
        labels: chartDataset.radarHour.labels,
        datasets: Object.entries(chartDataset.radarHour.data).reverse().reduce(function(datasetsArray, contactData) {
            datasetsArray.push({
                label: contactData[0],
                data: contactData[1].reduce(function(arr,hourCount,index){
                            arr.push({
                                x: index,
                                y: contactData[0],
                                r: hourCount
                            });
                            return arr;
                        }, []),
                borderColor: stringToColour(contactData[0]),
                backgroundColor: stringToColour(contactData[0]) + "80",
                fill: true,
            });
            return datasetsArray;
        }, [])
    };

    config = {
        type: 'radar',
        data: data,
        options: {
            scales: {
                r: {
                    //reverse: true,
                    stacked: true,
                    beginAtZero: true,
                    ticks: {
                        display: false
                    },
                },
            },
        }
    };
    chartsConfig.radarHour = config;





    // For mobile screens
    if (window.screen.availWidth <= 600) {
        Chart.defaults.maintainAspectRatio = false;
        Chart.defaults.plugins.legend.position = 'bottom';
    }

    return chartsConfig;
}








