
function convertDateFormat(theDate, dateDivision) {
    switch(dateDivision) {
        case 'days':   return theDate.toISOString().slice(0,10);
        case 'months': return theDate.toISOString().slice(0,7);
        case 'years':  return theDate.toISOString().slice(0,4);
    }
}
function dateTimeStep(theDate, dateDivision) {
    switch(dateDivision) {
        case 'days':   theDate.setDate(theDate.getDate()+1); break;
        case 'months': theDate.setMonth(theDate.getMonth()+1); break;
        case 'years':  theDate.setYear(theDate.getFullYear()+1); break;
    }
    return theDate;
}



function processData(smses, params) {


    // Apply filters
    smses = smses.filter(function(sms) {
        return (params.contacts == 'all') || params.contacts.includes(sms['contactName']);
    }).filter(function(sms) {
        dt = Number(sms['timestamp']);
        // sms timestamp is in milliseconds. JS Date object can be converted to timestamp with myDate.getTime()
        return ((params.dateStart=='default')||(dt>params.dateStart)) && ((params.dateEnd=='default')||(dt<params.dateEnd));
    });


    // get min and max dates
    var minAndMax = smses.reduce(function(prev, curr) {
        min = prev[0].timestamp < curr.timestamp ? prev[0] : curr;
        max = prev[1].timestamp > curr.timestamp ? prev[1] : curr;
        return [min,max];
    }, [smses[0],smses[0]]) ;
    minTime = Number(minAndMax[0].timestamp); maxTime = Number(minAndMax[1].timestamp);


    // define the text for "others"depending on the language
    var chartDataset = {};
    var pageLanguage = $('html').attr('lang');
    if (pageLanguage in dictionaries) {
        var allOthersString = dictionaries[pageLanguage]["allOthers"];
    }
    if (!allOthersString) { var allOthersString = "All others" }

    chartDataset.nTopContacts = params.nTopContacts;





    // count SMS per contact to get the category "others"
    var contact_no = smses.reduce(function(res, obj) {
        if (!(obj.contactName in res)) {
            res[obj.contactName] = 1;
        } else {
            res[obj.contactName] += 1;
        }
        return res;
    }, {})
    // sort the contacts from most to least SMSes
    var contactsCount = Object.entries(contact_no).sort(function(a,b) { return b[1] - a[1]; })
    // list the contacts presented individually
    var contactsList = contactsCount.reduce(function(cts,contact) { cts.push(contact[0]); return cts },[]);
    chartDataset.contactsList = contactsList;


    // create dataset for the top bar chart
    var labels = [];
    var data_array = [];
    for (tab of contactsCount.slice(0, params.nTopContacts + 1)){
        labels.push(tab[0]);
        data_array.push(tab[1]);
    }
    countAllOthers = contactsCount.slice(params.nTopContacts+1).reduce(function(res,obj) {return res + obj[1]}, 0);
    if (countAllOthers > 0) {
        labels.push(allOthersString);
        data_array.push(countAllOthers);
    }
    chartDataset.topBarChart = { labels: labels, data: data_array };




    // count SMS per contact per date, for the line chart. Dates are used as a string in ISO format or something close
    // first, create the dates list
    let dates = [];
    var defaultContactDate = {};
    var theDate = new Date(minTime);
    var maxTimeLoop = new Date(maxTime);
    maxTimeLoop = dateTimeStep(maxTimeLoop, params.timeDivision);
    while (theDate <= maxTimeLoop) {
        ISOdate = convertDateFormat(theDate, params.timeDivision)
        theDate = dateTimeStep(theDate, params.timeDivision);
        defaultContactDate[ISOdate] = 0;
        dates.push(ISOdate);
    }
    // create the Object[contact][date]
    var countContactDate = contactsList.slice(0, params.nTopContacts + 1).reduce(function(cts, contact) {
        cts[contact] = structuredClone(defaultContactDate);
        return cts;
    }, {[allOthersString]: structuredClone(defaultContactDate)} );



    var dataTimeline = {};
    // now read the data from smses
    dataTimeline = smses.reduce(function(dataTimeline, sms) {
        smsDate = new Date(Number(sms['timestamp']));
        smsDate = convertDateFormat(smsDate, params.timeDivision);
        // determine if contact in "others" category
        contact = contactsList.slice(0, params.nTopContacts + 1).includes(sms['contactName']) ? sms['contactName'] : allOthersString
        dataTimeline[contact][smsDate] += 1;
        return dataTimeline
    }, countContactDate);

    const cumulatedData = structuredClone(countContactDate);
    for (contact of Object.entries(dataTimeline)) {
        sum = 0
        for (datedata of Object.entries(contact[1])) {
            sum += datedata[1];
            cumulatedData[contact[0]][datedata[0]] = sum;
        }
    }

    chartDataset.timeLineChart =  { labels: dates, data: dataTimeline };
    chartDataset.cumulatedChart = { labels: dates, data: cumulatedData };



    // count SMS per hour of day
    // array of hours of day (0 to 23)
    const hours = [...Array(24).keys()];
    var defaultContactHour = new Array(24).fill(0);
    // create the Object[contact][hour]
    var countContactHour = contactsList.slice(0, params.nTopContacts + 1).reduce(function(cts, contact) {
        cts[contact] = structuredClone(defaultContactHour);
        return cts;
    }, {[allOthersString]: structuredClone(defaultContactHour)} );
    // populate that object
    nDays = dates.length;
    countContactHour = smses.reduce(function(countContactHour,sms) {
        smsDate = new Date(Number(sms['timestamp']));
        hour = smsDate.getHours();
        contact = contactsList.slice(0, params.nTopContacts + 1).includes(sms['contactName']) ? sms['contactName'] : allOthersString
        countContactHour[contact][hour] += 1/nDays
        return countContactHour;
    },countContactHour);

    chartDataset.hourOfDay =  { labels: hours, data: countContactHour };
    chartDataset.radarHour =  { labels: hours, data: countContactHour };


    // count SMS per day of week
    const days = [...Array(7).keys()];
    var defaultContactHour = new Array(7).fill(0);
    // create the Object[contact][day]
    var countContactDay = contactsList.slice(0, params.nTopContacts + 1).reduce(function(cts, contact) {
        cts[contact] = structuredClone(defaultContactHour);
        return cts;
    }, {[allOthersString]: structuredClone(defaultContactHour)} );
    // populate that object
    nWeeks = dates.length / 7 ;
    countContactDay = smses.reduce(function(countContactDay,sms) {
        smsDate = new Date(Number(sms['timestamp']));
        day = smsDate.getDay(); // Sunday = 0, Saturday = 6
        contact = contactsList.slice(0, params.nTopContacts + 1).includes(sms['contactName']) ? sms['contactName'] : allOthersString
        countContactDay[contact][day] += 1/nDays
        return countContactDay;
    },countContactDay);


    if (pageLanguage in dictionaries) {
        day_labels = days.reduce( function(arr,i) {
            arr.push(dictionaries[pageLanguage][ "dayOfWeek" + i ]); return arr;
        }, []);
    }
    // fall back to numbers in case of problem
    if ( (!day_labels)|| (day_labels.length == 0) || (!day_labels[0]) ) day_labels = days;

    chartDataset.radarWeek =  { labels: day_labels, data: countContactDay };


    return chartDataset
}





