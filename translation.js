


function browserLanguage(){
    /*Attention: returns a string , not the actual dictionary*/
    language = navigator.language.toLowerCase();
    langueCode = language.substring(0, 2);
    /*we only want the 2-letter language code*/
    if(Object.keys(dictionaries).indexOf(langueCode) != -1){
        return langueCode;
    }else{ return "en"; }
}

function translate(langue) {
    
    $(".translate").each(function(){
        try{
            $(this).html( dictionaries[langue][ $(this)[0]['id'] ] );
        } catch(e) { console.log(e); console.log("Mot non trouvé dans le dictionnaire"); };
    });
    // Change lang attribute for screen readers and/or google translate
    $('html').attr('lang', langue);
}



function loadDictionaries(){

    for (var langue of Object.keys(dictionaries)) {
        // Need to enclose the request code in a wrapper function, because of closure for variable langue
        (function(langue) {
            filename = "dictionaries/"+langue+".json";
            $.ajax({
                url : filename,
                dataType: "text",
                success : function (data) {
                    langDict = JSON.parse(data.replace(/(\r\n|\n|\r)/gm, ""));
                    dictionaries[langue] = langDict;
                },
                error: function (msg) { console.log( "error loading dictionary" + msg); }
            });
        })(langue)
    }

}


// langues implementees
var dictionaries = {"fr":{},"en":{}};
loadDictionaries();






